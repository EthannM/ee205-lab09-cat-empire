///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
///Ethan Mamuad <ethantm@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// 4/27/2021   
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>
#include <queue>

#define CAT_NAMES_FILE "names.txt"

#include "cat.hpp"

using namespace std;

Cat::Cat( const std::string newName ) {
	setName( newName );
}


void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );

	name = newName;
}


std::vector<std::string> Cat::names;


void Cat::initNames() {
	names.clear();
	cout << "Loading... ";

	ifstream file( CAT_NAMES_FILE );
	string line;
	while (getline(file, line)) names.push_back(line);

	cout << to_string( names.size() ) << " names." << endl;
}


Cat* Cat::makeCat() {
	// Get a random cat from names
	auto nameIndex = rand() % names.size();
	auto name = names[nameIndex];

	// Remove the cat (by index) from names
	erase( names, names[nameIndex] );

	return new Cat( name );
}

void CatEmpire:: addCat( Cat* newCat ){
if(topCat == nullptr){
   topCat = newCat;
   return;
}

addCat(topCat, newCat);

}


void CatEmpire::addCat( Cat* atCat, Cat* newCat ){

if(atCat -> name > newCat -> name){
   if(atCat->left == nullptr){
   atCat -> left = newCat;
   }
   else{
   addCat(atCat ->left, newCat);
   }

}


if(atCat -> name <  newCat -> name){
   if(atCat->right == nullptr){
   atCat -> right = newCat;
   }
   else{
   addCat(atCat ->right, newCat);
   }

}



}




bool CatEmpire::empty(){  // Return true if empty
if(topCat == nullptr){
   return true;
}

else{
   return false;
}

}


void CatEmpire::dfsInorderReverse( Cat* atCat, int depth ) const{
if(atCat == nullptr){
   return;
}

CatEmpire::dfsInorderReverse(atCat -> right, depth + 1);
cout << string( 6 * (depth-1), ' ' ) << atCat->name;
if((atCat ->right  == nullptr)&&(atCat -> left == nullptr)){

cout << endl;
}


if((atCat -> left != nullptr) && (atCat -> right != nullptr)){

cout << "<" << endl;


}

else if(atCat -> right != nullptr){

cout << "/" << endl;;
}

else if(atCat ->left != nullptr){

cout << "\\";
}
CatEmpire::dfsInorderReverse(atCat -> left, depth + 1);


}





void CatEmpire::catFamilyTree() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}



CatEmpire::dfsInorderReverse( topCat, 1 );


}




void CatEmpire::dfsInorder( Cat* atCat ) const{
if(atCat == nullptr){
   return;
}

   CatEmpire::dfsInorder(atCat->left);
   cout << atCat -> name << endl;
   CatEmpire::dfsInorder(atCat->right);

}


void CatEmpire::catList() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

dfsInorder( topCat );
}


void CatEmpire::dfsPreorder( Cat* atCat ) const{
if(atCat == nullptr){
   return;
}
if((atCat -> right != nullptr) && (atCat ->left != nullptr)){
cout << atCat -> name << " begat " << atCat ->left -> name << " and " << atCat -> right -> name << endl;
}

else if(atCat -> left != nullptr){
cout << atCat ->name << " begat " << atCat -> left -> name  << endl;
}

else if(atCat -> right != nullptr){
cout << atCat ->name << " begat " << atCat -> right -> name  << endl;
}


CatEmpire::dfsPreorder(atCat -> left);
CatEmpire::dfsPreorder(atCat -> right);


}


void CatEmpire::catBegat() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

dfsPreorder( topCat );
}

void CatEmpire::getEnglishSuffix(int n)const{
if(n == 2){
    cout << n << "nd " << "Generation" << endl;

}

else if (n == 3){
   cout << n << "rd " << "Generation" << endl;   
}
else{
   cout << n << "th " << "Generation" << endl;
}
}



void CatEmpire::catGenerations() const{

int count = 1;
CatEmpire::getEnglishSuffix(count);
cout<<"  " << topCat -> name << endl;
count ++;


 queue<Cat*> catQueue;
 catQueue.push(topCat);
 while(!catQueue.empty()){
 Cat* cat = catQueue.front(); // dequeue a cat…
 catQueue.pop();

//1st Generation
if(cat-> name == "Punkin"){
CatEmpire::getEnglishSuffix(count);
count++;
}


//2nd Generation
if(cat-> name == "Bernice"){
cout << endl;
CatEmpire::getEnglishSuffix(count);
count++;
}
//3rd Generation
if(cat-> name == "Alden"){
cout <<endl;
CatEmpire::getEnglishSuffix(count);
count++;
}
//4th Generation
if(cat-> name == "Adelphos"){
cout << endl;
CatEmpire::getEnglishSuffix(count);
count++;
}
//5th Generation
if(cat-> name == "Barrett"){
cout << endl;
CatEmpire::getEnglishSuffix(count);
count++;
}
//6th Generation
if(cat-> name == "Bowman"){
cout << endl;
CatEmpire::getEnglishSuffix(count);
count++;
}
//7th Generation
if(cat-> name == "Boswell"){
cout << endl;
CatEmpire::getEnglishSuffix(count);
count++;
}
//8th Generation
if(cat-> name == "Colton"){
cout << endl;
CatEmpire::getEnglishSuffix(count);
count++;
}
//9th Generation
if(cat-> name == "Carlos"){
cout << endl;
CatEmpire::getEnglishSuffix(count);
count++;
}

//10th Generation
if(cat-> name == "Clint"){
cout << endl;
CatEmpire::getEnglishSuffix(count);
count++;
}

//11th Generation
if(cat-> name == "Kyler"){
cout << endl;
CatEmpire::getEnglishSuffix(count);
count++;
}
//12th Generation
if(cat-> name == "Kingsley"){
cout << endl;
CatEmpire::getEnglishSuffix(count);
count++;
}
//13th Generation
if(cat-> name == "Lavender"){
cout << endl;
CatEmpire::getEnglishSuffix(count);
count++;
}
//14th Generation
if(cat-> name == "Nandor"){
cout << endl;
CatEmpire::getEnglishSuffix(count);
count++;
}
//15th Generation
if(cat-> name == "Nelson"){
cout << endl;
CatEmpire::getEnglishSuffix(count);
count++;
}
//16th Generation
if(cat-> name == "Narmada"){
cout << endl;
}


 if( cat->left != nullptr ){
 catQueue.push( cat->left );
 cout << "  " << cat-> left -> name;

 }
 if( cat->right != nullptr ){
 catQueue.push(cat->right); 
 cout << "  " <<  cat ->right -> name;
}

}
}



